from fastapi import FastAPI
import mock

app = FastAPI()

def buscador():
    indices = {}
    filter = {"violencia","estafa", "alimentos", "salud", "pension", "medicinas"}
    for tutela in mock.tutela:
        words = tutela["resumen"].split()
        for word in words:
            if word in filter:
                if word in indices:
                    indices[word].append(tutela)
                else:
                    indices[word] = [tutela]

    return indices

@app.get("/api/v1/search")
async def root():
      indices = buscador()
      resultado = {}
      for palabra, tutelas in indices.items():
        resultado[palabra] = [tutela["resumen"] for tutela in tutelas]

      return {"tutelas_por_palabra": resultado}




#@app.get("/hello/{name}")
#async def say_hello(name: str):
#   return {"message": f"Hello {name}"}
